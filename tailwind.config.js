module.exports = {
  content: ["./*.{html,js}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Poppins", "sans-serif"],
      },
      colors: {
        "blue-primary": "#0062FF",
        "gray-primary": "#25546F",
        "gray-secondary": "#25546F",
      },
    },
  },
  plugins: [],
};
